# testib puu loomiseks kuluvat aega

import random
import time
from prettytable import PrettyTable
import matplotlib.pyplot as plt

# https://pypi.org/project/leftrb/
import leftrb as rb
from leftrb.bst import BinarySearchTree
import sys

sys.setrecursionlimit(1000)


# parameetrid

maxKordusteArv = 10
juhuslik_jada = True
tippude_kasv = 9



failinimi = "treeTiming.txt"




def heightof(BST):
    return actual_height(BST.root)
def actual_height(bst_node):
    if bst_node is None:
        return 0
    else:
        return 1 + max(actual_height(bst_node.left), actual_height(bst_node.right))

def sizeof(BST):
    return actual_sizeof(BST.root)
def actual_sizeof(bst_node):
    if bst_node is None:
        return 0
    else:
        return actual_sizeof(bst_node.left) + 1 + actual_sizeof(bst_node.right)


def keskmine(endineKeskmine,lisatav,element):
    keskmine = (endineKeskmine * (element - 1) + lisatav) / element
    return(keskmine)


def test_tree(t,r):
    # t - puu
    # r - väärtused listina

    viga_create = 0
    viga_search = 0

    # loome puu
    try:
        algus = time.perf_counter()
        for x in r:
            t.insert(x)
        lopp = time.perf_counter()
        timing_tulemus = lopp - algus
    except RecursionError:
        timing_tulemus = 0
        viga_create = 1

    # otsime väärtused
    try:
        algus = time.perf_counter()
        res = [t.search(x) for x in r]
        lopp = time.perf_counter()
        otsing_tulemus = lopp - algus
    except RecursionError:
        otsing_tulemus = 0
        viga_search = 1
        res = ''

    if res == r:
        all_records_found = 1
    else:
        all_records_found = 0

    try:
        tree_height = heightof(t)
    except RecursionError:
        tree_height = 0

    return [timing_tulemus, otsing_tulemus, all_records_found, tree_height, viga_create, viga_search]





# teeme algandmed

# list tippude arvuga
tippe = [2]
for i in range(0,tippude_kasv):
    tippe.append(tippe[i]*2)


# Salvestame faili esimese rea
f = open(failinimi, "w")
f.write("Iteratsioon,Tippe,rb insert,rb search, rh height, bst insert, bst search, bst height\n")

f.close()

kordus = 1


rbinsert = []
bstinsert = []
rbsearch = []
bstsearch = []
rbheight = []
bstheight = []

while kordus <= maxKordusteArv:

    print()
    test = 1

    for i in range(len(tippe)):

        rbtree = rb.LeftRB()
        bsttree = BinarySearchTree()



        # kasvava listi saame nii:
        r = list(range(tippe[i]))

        if juhuslik_jada:
            # juhusliku listi saame nii
            random.shuffle(r)
            order = 'rnd'
            valim = 'Juhuslik'
        else:
            order = 'asc'
            valim = 'Järjestatud, kasvav.'



        # Test 1 lisame llrb puusse r väärtused
        print('Test nr: ' + str(kordus) + '.' + str(test) + '\t\tTippe: ' + str(tippe[i]) + '\t\t '+ order + ' create llrb: ' , end='')
        timingTulemus1 = test_tree(rbtree, r)
        if timingTulemus1[2] == 1:
            search = 'OK'
        else:
            search = 'NOK'

        print('create: ' + str(timingTulemus1[0]) + 's\t search: ' + str(timingTulemus1[1]) +' s\theight: ' + str(timingTulemus1[3]) + ' ' + search)

        test += 1

        # Test 2 lisame bst puusse r väärtused
        print('Test nr: ' + str(kordus) + '.' + str(test) + '\t\tTippe: ' + str(tippe[i]) + '\t\t '+ order + ' create BST: ' , end='')
        timingTulemus2 = test_tree(bsttree, r)

        if timingTulemus2[2] == 1:
            search = 'OK'
        else:
            search = 'NOK'


        if timingTulemus2[4] == 1 or timingTulemus2[5] == 1:
            print('Rekursiooni viga')
        elif timingTulemus2[4] == 0 and timingTulemus2[5] == 0:
            print('create: ' + str(timingTulemus2[0]) + 's\t search: ' + str(timingTulemus2[1]) +' s\theight: ' + str(timingTulemus2[3]) + ' ' + search)

        test += 1

        # lisame testi tulemused iga meetodi listi
        # kui on rohkem kui 1 kordust, siis arvutame keskmise

        if kordus == 1:
            rbinsert.append(timingTulemus1[0])
            rbsearch.append(timingTulemus1[1])
            rbheight.append(timingTulemus1[3])
            bstinsert.append(timingTulemus2[0])
            bstsearch.append(timingTulemus2[1])
            bstheight.append(timingTulemus2[3])

        else:
            rbinsert[i] = keskmine(rbinsert[i],timingTulemus1[0],kordus)
            rbsearch[i] = keskmine(rbsearch[i], timingTulemus1[1], kordus)
            rbheight[i] = keskmine(rbheight[i], timingTulemus1[3], kordus)

            bstinsert[i] = keskmine(bstinsert[i], timingTulemus2[0], kordus)
            bstsearch[i] = keskmine(bstsearch[i], timingTulemus2[1], kordus)
            bstheight[i] = keskmine(bstheight[i], timingTulemus2[3], kordus)



        # Salvestame read faili
        f = open(failinimi, "a")
        f.write(str(kordus) + "," + str(tippe[i]) + ',' \
                + str(timingTulemus1[0]) + ',' + str(timingTulemus1[1]) + ',' + str(timingTulemus1[3]) \
                + str(timingTulemus2[0]) + ',' + str(timingTulemus2[1]) + ',' + str(timingTulemus2[3]) \
                + '\n')
        f.close()

    kordus += 1

# teeme tulemustes null väärtused thjaks

for i in range(len(tippe)):
    if bstinsert[i] == 0:
        bstinsert[i] = None
        bstsearch[i] = None
        bstheight[i] = None


print('\n\n\n\n')

tabel = PrettyTable()

column_names = ["Tippe","llrb insert","bst insert","llrb search","bst search","llrb height", "bst height"]

tabel.add_column(column_names[0], tippe)
tabel.add_column(column_names[1], rbinsert)
tabel.add_column(column_names[2], bstinsert)
tabel.add_column(column_names[3], rbsearch)
tabel.add_column(column_names[4], bstsearch)
tabel.add_column(column_names[5], rbheight)
tabel.add_column(column_names[6], bstheight)
print(tabel)


plt.rcParams["figure.figsize"] = [12,12]

fig = plt.figure()
fig.suptitle("Red-Black tree ja lihtsa binary search tree võrdlus ", x=0.5, y=.95, horizontalalignment='center', verticalalignment='top', fontsize = 15)

plt.subplot(311)
plt.plot(tippe, rbinsert, label=column_names[1])
plt.plot(tippe, bstinsert, label=column_names[2])
#plt.xlabel('Tippe')
plt.ylabel('Aeg (s)')
#plt.yscale('log')
plt.title('Lisamine').set_position([0.5, 0.9])
plt.legend(loc=0)


plt.subplot(312)
plt.plot(tippe, rbsearch, label=column_names[3])
plt.plot(tippe, bstsearch, label=column_names[4])
#plt.xlabel('Tippe')
plt.ylabel('Aeg (s)')
#plt.yscale('log')
plt.title('Otsing').set_position([0.5, 0.9])
plt.legend(loc=0)

plt.subplot(313)
plt.plot(tippe, rbheight, label=column_names[5])
plt.plot(tippe, bstheight, label=column_names[6])
plt.xlabel('Tippe')
plt.ylabel('Suurim sügavus')
#plt.yscale('log')
plt.title('Kõrgus').set_position([0.5, 0.9])
plt.legend(loc=0)


# plt.text(0, -1, 'Katsete arv = ' + str(maxKordusteArv) + '. ' + valim)
plt.show()

#
#
# plt.xlabel('Tippe')
# plt.ylabel('Aeg (s)')
# #plt.yscale('log')
# plt.title('Puu loomise aeg')
# plt.legend(loc=2)
# plt.show()