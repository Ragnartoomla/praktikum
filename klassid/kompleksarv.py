import math
import numbers
import os

# Python initsialiseerib hash funktsiooni igal käivitamisel uuesti, mille tõttu on iga kord hash funktsiooni tulemus erinev
# selleks, et seda ei juhtuks, on vaja keskkonnamuutuja ette anda
os.environ['PYTHONHASHSEED'] = '0'

class Kompleksarv(object):

    EPSILON = 0.0000001

    # klassimeetod, mis tagastab kompleksarvu, mida kirjeldab parameetriks olev tekst.
    # Peab olema kooskõlas meetodiga __str__ (tõlgendama __str__ poolt väljastatud teksti õigesti)
    # ning vigase teksti korral tekitama erindi ValueError, mille veateatest nähtuksid nii vigane tekst
    # kui ka vea põhjus.
    @staticmethod
    def parse_kompleksarv(tekst):
        try:
            arv = complex(tekst.replace("i", "j"))
            return(Kompleksarv(arv.real,arv.imag))
        except ValueError:
            raise ValueError ('Viga. Sisend ei ole kompleksarvule sobivas formaadis: "' + str(tekst) + '"')


    # konstruktor, mille parameetriteks on kaks reaalarvu - kompleksarvu reaalosa ja imaginaarosa
    def __init__(self, re, im):
        if (isinstance(re, numbers.Real) == True) and (isinstance(im, numbers.Real) == True):
            self.re = re
            self.im = im
        else:
            raise TypeError('Parameetrid peavad olema reaalarvud! Reaalosa: "' + str(re) + '" Imaginaarosa: "' + str(im) + '"')

    # predikaat, mis on tõene, kui kompleksarv ei võrdu nulliga
    def __bool__(self):
        if self.re == 0  and self.im == 0:
            return False
        else:
            return True

    # predikaat, mis on tõene, kui kompleksarvud self ja teine on võrdsed
    # def __eq__(self, teine):
    #     if self.re == teine.re and self.im == teine.im:
    #         return True
    #     else:
    #         return False

    # predikaat, mis on tõene, kui kompleksarvud self ja teine on võrdsed
    def __eq__(self, teine):
        if abs(self.re - teine.re) < Kompleksarv.EPSILON and abs(self.im - teine.im) < Kompleksarv.EPSILON:
            return True
        else:
            return False



    # meetod, mis tagastab kompleksarvu tekstilise kuju: sõne kujul "a+bi" või "a-bi" vastavalt imaginaarosa b märgile
    # kui imaginaarosa ei ole, siis tagastab ainult reaalosa
    def __str__(self):
        if self.im != 0 and self.re != 0:
            return str('{:-g}'.format(self.re)) + str('{:+g}'.format(self.im)) + "i"

        elif self.re == 0 and self.im != 0:
            return str('{:-g}'.format(self.im)) + "i"
        else:
            return str('{:-g}'.format(self.re))

    # meetod, mis tagastab kompleksarvu räsi - täisarvu, mis sõltub nii reaalosast kui ka imaginaarosast, on võrdsete
    # kompleksarvude jaoks sama ning mittevõrdsete kompleksarvude jaoks üldjuhul erinev
    # selleks, et hash ei muutuks on vaja muuta enviroment variable PYTHONHASHSEED=0
    def __hash__(self):
        return hash(str(self.re) + str(self.im))

    # meetod, mis tagastab kompleksarvu reaalosa re
    def reaalosa(self):
        return self.re

    # meetod tagastab kompleksarvu imaginaarosa im
    def imaginaarosa(self):
        return self.im

    # tagastab kompleksarvu kaaskompleksarvu, mis avaldub valemiga: kaaskompleksarv(a + bi) = a - bi
    def kaaskompleksarv(self):
        return Kompleksarv(self.re,self.im * -1)

    # meetod mis tagastab kompleksarvude self ja teine summa, mi avaldub valemiga (a+bi) + (c+di) = (a+c) + (b+d)i.
    def __add__(self, teine):
        return Kompleksarv(self.re + teine.re, self.im + teine.im)

    # meetod, mis tagastab kompleksarvu vastandarvu, mis avaldub valemiga vastandarv(a+bi) = -a-bi
    def __neg__(self):
        return Kompleksarv(self.re * -1, self.im * -1)

    # meetod, mis tagastab kompleksarvude self ja teine korrutise: mis avaldub valemiga
    # (a+bi) * (c+di) = (ac-bd) + (ad+bc)i
    def __mul__(self, teine):
        return Kompleksarv((self.re * teine.re - self.im * teine.im),(self.re * teine.im + self.im * teine.re))

    # meetod, mis tagastab kompleksarvu pöördarvu, mis avaldub valemiga 1/(a+bi) = a/(a*a+b*b) + ((-b)/(a*a+b*b))i
    def poordarv(self):
        if self:
            return Kompleksarv(self.re / (self.re * self.re + self.im * self.im),((-self.im) / (self.re * self.re + self.im * self.im)))
        else:
            raise ZeroDivisionError('Nulliga jagamine ei ole lubatud')

    # meetod, mis tagastab kompleksarvude self ja teine vahe. Lahutamine on vastandarvu liitmine
    def __sub__(self, teine):
        return self + (-teine)

    # meetod, mis tagastab kompleksarvude self ja teine jagatise. Jagamine on pöördarvuga korrutamine
    def __truediv__(self, teine):
         return self * Kompleksarv.poordarv(teine)

    # meetod, mis tagastab kompleksarvu mooduli, mis avaldub valemiga moodul(a+bi) = sqrt(a*a + b*b)
    def moodul(self):
        return math.sqrt(self.re * self.re + self.im * self.im)

    # meetod, mis tagastab kompleksarvu 0.
    # Avaldub seosest a+bi = m*cos(p) + (m*sin(p))i, kus m on moodul ja p on argumendi peaväärtus
    def nurk(self):

        x = self.re
        y = self.im

        # leiame argumendi peaväärtuse (http://staff.ttu.ee/~alar/KMF/loeng.pdf lk 7)
        if x > 0:
            p = math.atan(y/x)
        elif x < 0 and y >= 0:
            p = math.pi + math.atan(y/x)
        elif x < 0 and y < 0:
            p = -math.pi + math.atan(y/x)
        elif x == 0 and y > 0:
            p = math.pi / 2
        elif x == 0 and y < 0:
            p = -math.pi / 2
        else:
            #peaväärtust ei defineerita
            raise ArithmeticError('Nullist ei saa nurka arvutada')
            p = 'na'

        return(p)

    # õppejõu variant
    def nurk2(self):

        if self:
            return  math.atan2(self.im, self.re)
        else:
            #peaväärtust ei defineerita
            raise ArithmeticError('Nullist ei saa nurka arvutada')



#
#
# for i in range(0, 360):
#     c1 = Kompleksarv(math.cos(math.radians(i)), math.sin(math.radians(i)))
#     m = c1.moodul()
#     p = c1.nurk()
#     print(str(c1) + " \t" + str((Kompleksarv(m * math.cos(p), m * math.sin(p)))))
