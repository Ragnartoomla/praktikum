#!/usr/bin/env python3

import kompleksarv
import unittest
import math


class MyTestCase(unittest.TestCase):

    def test_plus(self):
        res = kompleksarv.Kompleksarv(2, 5).__add__(kompleksarv.Kompleksarv(4, 15))
        self.assertEqual(kompleksarv.Kompleksarv(6, 20), res, "liitmise viga")
        res = kompleksarv.Kompleksarv(-2, -5).__add__(kompleksarv.Kompleksarv(4, 15))
        self.assertEqual(kompleksarv.Kompleksarv(2, 10), res, "liitmise viga")

    def test_minus(self):
        res = kompleksarv.Kompleksarv(2, 5).__sub__(kompleksarv.Kompleksarv(4, 15))
        self.assertEqual(kompleksarv.Kompleksarv(-2, -10), res, "lahutamise viga")
        res = kompleksarv.Kompleksarv(-2, -5).__sub__(kompleksarv.Kompleksarv(4, 15))
        self.assertEqual(kompleksarv.Kompleksarv(-6, -20), res, "lahutamise viga")

    def test_mult(self):
        res = kompleksarv.Kompleksarv(4, -3).__mul__(kompleksarv.Kompleksarv(-24, 7))
        self.assertEqual(kompleksarv.Kompleksarv(-75, 100), res, "korrutamise viga")
        res = kompleksarv.Kompleksarv(1, -1).__mul__(kompleksarv.Kompleksarv(3, 7))
        self.assertEqual(kompleksarv.Kompleksarv(10, 4), res, "korrutamise viga")

    def test_div(self):
        res = kompleksarv.Kompleksarv(-75, 100).__truediv__(kompleksarv.Kompleksarv(-24, 7))
        self.assertEqual(kompleksarv.Kompleksarv(4, -3), res, "jagamise viga")
        res = kompleksarv.Kompleksarv(10, 4).__truediv__(kompleksarv.Kompleksarv(1, -1))
        self.assertEqual(kompleksarv.Kompleksarv(3, 7), res, "jagamise viga")

    def test_div_by_zero(self):
        with self.assertRaises(Exception, msg="nulliga jagamine peaks olema viga"):
            kompleksarv.Kompleksarv(3, 4).__truediv__(kompleksarv.Kompleksarv(0, 0))

    def test_nurk_null(self):
        with self.assertRaises(Exception, msg="nullist nurga leidmine peaks olema viga"):
            kompleksarv.Kompleksarv(0, 0).nurk()

    def test_nurk(self):
        for i in range(0, 360):
            c1 = kompleksarv.Kompleksarv(math.cos(math.radians(i)), math.sin(math.radians(i)))
            m = c1.moodul()
            p = c1.nurk()
            self.assertEqual(c1, kompleksarv.Kompleksarv(m*math.cos(p), m*math.sin(p)), "nurga viga " + str(c1))


if __name__ == '__main__':
    unittest.main()