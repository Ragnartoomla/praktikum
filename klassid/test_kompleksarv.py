from unittest import TestCase
import kompleksarv as k
import math
import os
# Python initsialiseerib hash funktsiooni igal käivitamisel uuesti, mille tõttu on iga kord hash funktsiooni tulemus erinev
# selleks, et seda ei juhtuks, on vaja keskkonnamuutuja ette anda
os.environ['PYTHONHASHSEED'] = '0'


class TestKompleksarv(TestCase):
    # testib kas tekstiline kuju on õige
    def test_str(self):
        self.assertEqual('1+2i', str(k.Kompleksarv(1,2)))
        self.assertEqual('0', str(k.Kompleksarv(0,0)))
        self.assertEqual('0', str(k.Kompleksarv(0,-0)))
        self.assertEqual('-1', str(k.Kompleksarv(-1,0)))
        self.assertEqual('-2i', str(k.Kompleksarv(0,-2)))
        self.assertEqual('1-2i', str(k.Kompleksarv(1,-2)))

    # testib kas tekib viga kui parameetrid ei ole reaalarvud
    def test_init_typeError(self):
        with self.assertRaises(TypeError):
            k.Kompleksarv("a", 1)
            k.Kompleksarv(1, "a")
            k.Kompleksarv(True, 1)
            k.Kompleksarv(1, False)

    # testib kas on tõene kui kompleksarv ei võrdu nulliga ja väär, kui võrdub nulliga
    def test_boolean(self):
        self.assertTrue(k.Kompleksarv(1, 1))
        self.assertTrue(k.Kompleksarv(0, 1))
        self.assertTrue(k.Kompleksarv(1, 0))
        self.assertFalse(k.Kompleksarv(0, 0))

    # testib reaalosa tagastamist
    def test_reaalosa(self):
        self.assertEqual(1, k.Kompleksarv.reaalosa(k.Kompleksarv(1,2)))
        self.assertEqual(-1, k.Kompleksarv.reaalosa(k.Kompleksarv(-1,2)))
        self.assertEqual(0, k.Kompleksarv.reaalosa(k.Kompleksarv(0,0)))

    # testib imaginaarosa tagastamist
    def test_imaginaarosa(self):
        self.assertEqual(2, k.Kompleksarv.imaginaarosa(k.Kompleksarv(1,2)))
        self.assertEqual(-2, k.Kompleksarv.imaginaarosa(k.Kompleksarv(1,-2)))
        self.assertEqual(0, k.Kompleksarv.imaginaarosa(k.Kompleksarv(0,0)))

    # testib kaaskompleksarvu tagastamist
    def test_kaaskompleksarv(self):
        self.assertEqual('1-2i', str(k.Kompleksarv.kaaskompleksarv(k.Kompleksarv(1, 2))))
        self.assertEqual('1+2i', str(k.Kompleksarv.kaaskompleksarv(k.Kompleksarv(1, -2))))
        self.assertEqual('0', str(k.Kompleksarv.kaaskompleksarv(k.Kompleksarv(0, 0))))
        self.assertEqual('1i', str(k.Kompleksarv.kaaskompleksarv(k.Kompleksarv(0, -1))))

    # testib pöördarvu tagastamist
    def test_poordarv(self):
        self.assertEqual('0.2-0.4i', str(k.Kompleksarv.poordarv(k.Kompleksarv(1, 2))))
        self.assertEqual('0.2+0.4i', str(k.Kompleksarv.poordarv(k.Kompleksarv(1, -2))))

    def test_poordarv_zerodivisionerror(self):
        with self.assertRaises(ZeroDivisionError):
            k.Kompleksarv.poordarv(k.Kompleksarv(0, 0))

    # kompleksarvu mooduli test
    def test_moodul(self):
        self.assertEqual(math.sqrt(5), k.Kompleksarv.moodul(k.Kompleksarv(1,2)))

    # testib hash funktsiooni
    # selleks, et hash ei muutuks on vaja muuta enviroment variable PYTHONHASHSEED=0
    def test_hash(self):
        self.assertEqual('3462651383276316179', str(hash(k.Kompleksarv(4,6))))


    # testime võrdsuse kontrolli
    def test_eq(self):
        a = k.Kompleksarv(4, 6)
        b = k.Kompleksarv(4, 6)
        c = k.Kompleksarv(8, 12)
        d = k.Kompleksarv(4, -6)
        self.assertTrue(a == b)
        self.assertFalse(a == c)
        self.assertFalse(a == d)

    # liitmine
    def test_add(self):
        self.assertEqual('6', str(k.Kompleksarv(3, 2) + k.Kompleksarv(3, -2)))
        self.assertEqual('2+4i', str(k.Kompleksarv(1, 2) + k.Kompleksarv(1, 2)))
        self.assertEqual('0', str(k.Kompleksarv(0, 0) + k.Kompleksarv(0, 0)))

    # lahutamine
    def test_sub(self):
        self.assertEqual('6', str(k.Kompleksarv(9, 2) - k.Kompleksarv(3, 2)))
        self.assertEqual('0', str(k.Kompleksarv(1, 2) - k.Kompleksarv(1, 2)))
        self.assertEqual('4i', str(k.Kompleksarv(1, 2) - k.Kompleksarv(1, -2)))
        self.assertEqual('0', str(k.Kompleksarv(0, 0) - k.Kompleksarv(0, 0)))

    # korrutamine
    def test_mul(self):
        self.assertEqual('13', str(k.Kompleksarv(3, 2) * k.Kompleksarv(3, -2)))
        self.assertEqual('5+12i', str(k.Kompleksarv(3, 2) * k.Kompleksarv(3, 2)))
        self.assertEqual('0', str(k.Kompleksarv(0, 0) * k.Kompleksarv(3, 2)))
        self.assertEqual('-4+2i', str(k.Kompleksarv(0, 2) * k.Kompleksarv(1, 2)))
        self.assertEqual('6i', str(k.Kompleksarv(3, 0) * k.Kompleksarv(0, 2)))

    # kompleksarvu vastandarv
    def test_neg(self):
        self.assertEqual('-1-2i', str(-k.Kompleksarv(1, 2)))
        self.assertEqual('1+2i', str(-k.Kompleksarv(-1, -2)))

    # jagamine
    def test_truediv(self):
        self.assertEqual('0.5', str(k.Kompleksarv(1, 1) / k.Kompleksarv(2, 2)))
        self.assertEqual('1i', str(k.Kompleksarv(1, 1) / k.Kompleksarv(1, -1)))  # küsi järgi, mis on õige (i või 1i) https://www.hackmath.net/en/calculator/complex-number?input=%281%2B1i%29%2F%281-1i%29&submit=Calculate

    # jagamine nulliga
    def test_truediv_zerodivisionerror(self):
        with self.assertRaises(ZeroDivisionError):
            k.Kompleksarv(1, 1) / k.Kompleksarv(0, 0)

    # teisendamine stringist
    def test_parse_kompleksarv(self):
        self.assertEqual('1-2i', str(k.Kompleksarv.parse_kompleksarv("1-2i")))
        self.assertEqual('0', str(k.Kompleksarv.parse_kompleksarv("0")))
        self.assertEqual('0', str(k.Kompleksarv.parse_kompleksarv("0+0i")))
        self.assertEqual('-2i', str(k.Kompleksarv.parse_kompleksarv("0-2i")))
        self.assertEqual('2i', str(k.Kompleksarv.parse_kompleksarv("0+2i")))
        self.assertEqual('1', str(k.Kompleksarv.parse_kompleksarv("1+0i")))

    # teisendamine stringist ebasobiv sisend
    def test_parse_kompleksarv_input(self):
        with self.assertRaises(ValueError):
            k.Kompleksarv.parse_kompleksarv("midagi muud")
            k.Kompleksarv.parse_kompleksarv("")

    # testib polaarkuju argumendi peaväärtuse arvutamist
    def test_nurk1(self):
        self.assertEqual(math.pi / 12, k.Kompleksarv.nurk(k.Kompleksarv(2 + math.sqrt(3), 1)))
        self.assertEqual(-math.pi / 4, k.Kompleksarv.nurk(k.Kompleksarv(1, -1)))
        self.assertEqual(-3 / 4 * math.pi, k.Kompleksarv.nurk(k.Kompleksarv(-1, -1)))
        self.assertEqual(math.pi / 2, k.Kompleksarv.nurk(k.Kompleksarv(0, 1)))
        self.assertEqual(-math.pi / 2, k.Kompleksarv.nurk(k.Kompleksarv(0, -1)))
        #self.assertEqual('0', str(k.Kompleksarv.nurk(k.Kompleksarv(0, 0))))

    def test_nurk_null(self):
        with self.assertRaises(Exception, msg="nullist nurga leidmine peaks olema viga"):
            k.Kompleksarv(0, 0).nurk()

    def test_nurk(self):
        for i in range(0, 360):
            c1 = k.Kompleksarv(math.cos(math.radians(i)), math.sin(math.radians(i)))
            m = c1.moodul()
            p = c1.nurk()
            self.assertAlmostEqual(c1, k.Kompleksarv(m*math.cos(p), m*math.sin(p)), places=5, msg=("nurga viga " + str(c1)))

            #assertAlmostEqual(first, second, places=7, msg=None, delta=None)