#!/usr/bin/env python3
import sys



class Magasin(object):

    __list = []

    @staticmethod
    def interpret(avaldis):

        # Interpreteerib aritmeetilise avaldise pööratud poola kuju(sulgudeta postfikskuju, Reverse Polish Notation)
        # Sisend 'avaldis' on antud stringina, mis võib sisaldada arve (s.h. negatiivseid ja mitmekohalisi) ning
        # tehtemärke + - * / , mis on eraldatud tühikutega (whitespace).
        # Tulemuseks on avaldise väärtus arvuna või Exception, kui avaldis ei ole korrektne.
        # Korrektne ei ole, kui avaldises esineb lubamatuid sümboleid, kui avaldis jätab magasini üleliigseid
        # elemente või kasutab magasinist liiga palju elemente.

        avaldisList = avaldis.split()
        tehted = ['+', '-', '*', '/']
        stack = Magasin([])

        # käime läbi kõik väärtused etteantud avaldises
        for i in range(0, len(avaldisList)):
            if avaldisList[i] in tehted and len(stack) > 1:
                # kui on tehtemärk ja  stackis on vähemalt 2 elementi teeme tehingu
                stack.push(stack.op(avaldisList[i]))

            elif avaldisList[i] in tehted and len(stack) <= 1:
                # kui on tehtemärk aga stackis on vähem kui 2 elementi anname vea
                raise ValueError('Lubamatu sümbol "' + str(avaldisList[i]) + '" avaldises positsioonil ' + str(i) + '. Oodatakse numbrit')

            else:
                # kui ei olnud tehtemärk, siis proovime teha numbriks ja lisada stacki. Kui ei õnnestu, siis viga.
                try:
                    stack.push(float(avaldisList[i]))
                except:
                    raise ValueError('Lubamatu sümbol "' + str(avaldisList[i]) + '" avaldises.')

        if len(stack) == 1:
            # Kui lõpuks on stackis 1 element, siis aname vastuse
            return stack.tos()
        elif len(stack) == 0:
            raise ValueError('Tühi avaldis')
        else:
            # Kui ei ole 1 element, siis järelikult on mõni tehtemärk puudu
            raise ValueError('Avaldises on puudu õige arv tehtemärke.')





    def __init__(self, lst=None):
        if lst is None:
            self.__list = []
        else:
            self.__list = lst

    def __len__(self):
        return len(self.__list)

    def tyhi(self):
        return len(self) == 0

    def push(self, value):
        self.__list.append(value)

    def pop(self):
        if self.tyhi():
            raise IndexError("pop tyhjast magasinist")
        return self.__list.pop()

    def tos(self):
        if self.tyhi():
            raise IndexError("tos tyhjast magasinist")
        return self.__list[len(self.__list)-1]

    def __bool__(self):
        return not self.tyhi()

    def __str__(self):
        res = ""
        for k in self.__list:
            res += " " + str(k)
        if res == "":
            res = "tyhi"
        return res

    def __eq__(self, other):
        if len(self) != len(other):
            return False
        for k in range(0, len(self)):
            if self.__list[k] != other.__list[k]:
                return False
        return True

    def copy(self):
        return Magasin(list(self.__list))

    def op(self, operator):
        if self.tyhi():
            raise IndexError("op tyhjast magasinist")
        o2 = self.pop()
        if self.tyhi():
            raise IndexError("op pooltyhjast magasinist")
        o1 = self.pop()
        if operator.strip() in ['+', '-', '*', '/']:
            res = eval(str(o1) + operator + str(o2))
        else:
            raise ValueError("vale tehe " + operator + " meetodis op")
        return res


