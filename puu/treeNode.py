#!/usr/bin/env python3
"""
Module: treeNode.py
General trees.
"""

import sys
import random
import math
import time


__author__ = 'Jaanus'


class Tree_node(object):
    """
    Class Node.
    Tree node with two pointers.
    """
    __name = 'noname_node'
    __first_child = None
    __next_sibling = None
    __info = 0

    def __init__(self, name, down, right):
        self.__name = name
        self.__first_child = down
        self.__next_sibling = right

    def __str__(self):
        return self.show_tree()

    def get_name(self):
        return self.__name

    def set_name(self, name):
        self.__name = name

    def get_first_child(self):
        return self.__first_child

    def set_first_child(self, node):
        self.__first_child = node

    def get_next_sibling(self):
        return self.__next_sibling

    def set_next_sibling(self, node):
        self.__next_sibling = node

    def get_info(self):
        return self.__info

    def set_info(self, info):
        self.__info = info

    def process_node(self):
        print(self.get_name(), end=' ')

    def pre_order(self):
        self.process_node()
        child = self.get_first_child()
        while child:
            child.pre_order()
            child = child.get_next_sibling()

    def show_tree(self):
        # Meetod tagastab puu vasakpoolse suluesituse stringina (puu juureks on tipp self).
        # See meetod tulemust ei trüki, ainult tagastab sõnetüüpi väärtuse. Testige ka ühetipuline puu.
        # Testpuude moodustamine ja tulemuse väljatrükk olgu peameetodis.
        # Puu kujutamisviisina kasutage loengul esitatud viidastruktuuri.

        res = ''
        res += str(self.get_name())  # tipu väärtus vastusesse
        child = self.get_first_child()

        if child:  # kui on alluv, siis lisame sulgude alguse
            res += '('

        while child:  # käime läbi tsükli iga alluva kohta
            res += child.show_tree()  # rekursioon annab tagasi vastuse alluva puu kohta
            child = child.get_next_sibling()
            if child:  # kui on olemas samal tasemel teine laps, siis paneme koma vahele
                res += ', '
            else:  # kui rohkem lapsi samal tasemel ei ole, siis sulud kinni
                res += ')'

        return res

    def add_child(self, tipp):
        if tipp:
            if not self.get_first_child():
                self.set_first_child(tipp)
            else:
                laps = self.get_first_child()
                while laps.get_next_sibling():
                    laps = laps.get_next_sibling()
                laps.set_next_sibling(tipp)

    @staticmethod
    def create_tree():
        return Tree_node('A',
                         Tree_node('B',
                                   None,
                                   Tree_node('C',
                                             Tree_node('D',
                                                       None,
                                                       None),
                                             Tree_node('E',
                                                       None,
                                                       None))),
                         None)

    def random_tree(tippe, laius, parent='1'):

        # klassimeetod juhusliku puu moodustamiseks,
        # milles on juhuslik arv tippe vahemikus 1 kuni max_tippe (proovige umbes 20 tipuga) ning igal vahetipul on
        # ülimalt max_laius alluvat. Peameetodis genereerige 10 juhuslikku puud ning
        # väljastage nende vasakpoolsed suluesitused. Tippude nimed valige nii, et need kajastaksid tipu asukohta
        # puus (näit. 1, 1.1, 1.2, 1.1.1, 1.1.2 jne).
        #
        # sisendid:
        # tippe (int) - tippude maksimaalne arv
        # laius (int) - maksimaalne laius
        # parent (str) - puu tipu väärtus, võib ka mitte anda, vaikimisi 1

        # kontrollime sisendeid
        if tippe < 1 and parent == '1': # väike häkk. kui on juurtipp, siis ei luba 0 tippu.
            raise ValueError(('Lubamatu sümbol "' + str(tippe) + '". Tippude arv peab olema vähemalt 1.'))

        if not tippe >= 0 or not isinstance(tippe, int):
            raise ValueError(('Lubamatu sümbol "' + str(tippe) + '". Tippude arv peab olema täisarv.'))

        if not laius > 0 or not isinstance(laius, int):
            raise ValueError(('Lubamatu sümbol "' + str(laius) + '". Puu laius arv peab olema täisarv.'))

        puu = Tree_node(parent, None, None)
        rand_laius = random.randint(1, laius)  # valime juhuslikult tipu alluvate arvu

        rand_tippe = Tree_node.distribute_nodes(random.randint(1, tippe) - 1, rand_laius)  # valime juhusliku tippude arvu ja jagame ülejäänud tipud alluvate vahel ära

        # testimiseks tippude arv on alati etteantud maksimaalne väärtus
        #rand_tippe = Tree_node.distribute_nodes(tippe - 1, rand_laius)

        for i in range(0,len(rand_tippe)):
            nimi = str(parent) + '.' + str(i + 1)
            if rand_tippe[i] == 1:  # kui sellesse harusse ei tule alampuid, siis lisame lehed
                puu.add_child(Tree_node(nimi, None, None))

            elif rand_tippe[i] > 1:  # kui võib lisada alluvaid tasemeid, siis genereerime laiuse ulatuses rekursiooniga uusi harusid
                puu.add_child(Tree_node.random_tree(rand_tippe[i], laius, nimi))


        return puu

    def distribute_nodes(tippe_jagada, lubatud_laius):
        # funktsioon jagab tipud etteantud laiuse vahel juhuslikult
        tippude_jaotus = []
        tippe_jarel = tippe_jagada

        for i in range(lubatud_laius):
            if tippe_jarel > 1 and not i == lubatud_laius - 1:
                n = random.randint(1, tippe_jarel)
                tippe_jarel -= n
                tippude_jaotus.append(n)
            elif tippe_jarel > 0 and i == lubatud_laius - 1:  # viimasele elemendile lisame järelejäänud tipud
                tippude_jaotus.append(tippe_jarel)
            # else:
            #     tippude_jaotus.append(0)

        random.shuffle(tippude_jaotus)

        return (tippude_jaotus)


def main():
    """
    Main method.
    """

    # puu suluesitus

    puu1 = Tree_node('1',
                     Tree_node('1.1',
                               Tree_node('1.1.1',
                                         None,
                                         Tree_node('1.1.2',
                                                   None,
                                                   None)),
                               Tree_node('1.2',
                                         Tree_node('1.2.1',
                                                   None,
                                                   Tree_node('1.2.2',
                                                             None,
                                                             None)),
                                         None)
                               ),
                     None)

    puu2 = Tree_node('A', None, None)
    puu3 = Tree_node.random_tree(2,40)


    puu4 = Tree_node.create_tree()

    print(puu4)

    # print(puu2)
    # print(puu3)
    # print()
    #
    for i in range(1, 11):
         print(str(i) + '\t' + str(Tree_node.random_tree(20, 4)))


if __name__ == '__main__':
    main()
