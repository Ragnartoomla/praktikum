# testib puu loomiseks kuluvat aega

import random
import time
import treeNode
from prettytable import PrettyTable
import matplotlib.pyplot as plt





def Tee_puu(tippe,laius):
    algus = time.perf_counter()
    a = treeNode.Tree_node.random_tree(tippe, laius)
    lopp = time.perf_counter()
    return lopp-algus


def keskmine(endineKeskmine,lisatav,element):

    keskmine = (endineKeskmine * (element - 1) + lisatav) / element

    return(keskmine)

# parameetrid

maxKordusteArv = 3

failinimi = "treeTiming.txt"
# teeme algandmed

tippe = [2]
for i in range(1,17):
    tippe.append(tippe[i-1]*2)

laiused = [2, 4, 6, 8]

koond = []
for i in range(len(laiused)): # create a list with nested lists
     koond.append('')

print(koond)

# Salvestame faili esimese rea
f = open(failinimi, "w")
f.write("Iteratsioon,Tippe,Laius,Timing\n")

f.close()

test = 1
for j in range(len(laiused)):
    tulemus = []
    kordus = 1
    while kordus <= maxKordusteArv:
        print()
        for i in range(len(tippe)):
            print('Test nr: ' + str(test) + '.' + str(kordus) + '. Laius: ' + str(laiused[j]) + '. Tippe: ' + str(tippe[i]) + '...', end='')
            test += 1
            timingTulemus = Tee_puu(tippe[i],laiused[j])
            print("Done")
            #print(timingTulemus)

            # lisame testi tulemused iga meetodi listi
            # kui on rohkem kui 1 kordust, siis arvutame keskmise
            if kordus > 1:
                tulemus[i] = keskmine(tulemus[i],timingTulemus,kordus)
            else:
                tulemus.append(timingTulemus)

            # Salvestame rea faili
            f = open(failinimi, "a")
            f.write(str(test) + "," + str(tippe[i]) + "," + str(laiused[j]) + ',' + str(timingTulemus) + '\n')
            f.close()
        kordus += 1
    koond[j] = tulemus





tabel = PrettyTable()

column_names = ["Tippe"]
for i in range(len(laiused)):
    column_names.append('Laius ' + str(laiused[i]))

tabel.add_column(column_names[0], tippe)
for i in range(len(laiused)):
     tabel.add_column(column_names[i+1], koond[i])


print(tabel)


for i in range(len(laiused)):
    joon = 'Laius ' + str(laiused[i])
    plt.plot(tippe, koond[i], label=joon)


plt.xlabel('Tippe')
plt.ylabel('Aeg (s)')
#plt.yscale('log')
plt.title('Puu loomise aeg')
plt.legend(loc=2)
plt.show()