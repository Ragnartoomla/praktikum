# Järjestamismeetodid
#
# Korraldada "aus võistlus" erinevate järjestamismeetodite vahel,
# mõõtes lahendamisaega ühe ja sama juhusliku mittenegatiivsete
# täisarvude massiivi järjestamiseks erinevate meetoditega:
# pistemeetodil, kahendpistemeetodil, kiirmeetodil, ühildamismeetodil ning APIs sisalduva meetodiga.
# Ajamõõtmiseks võib kasutada meetodit time.perf_counter()
# Andmemahud olgu näiteks  4000, 8000, 16000 ja 32000 elementi (aeglase arvuti korral võite võtta vähem elemente,
# kiire korral rohkem). Koostada lahendusaegadest tabel ja joongraafik, millel paigutada
# samasse teljestikku kõik viis joont
# (lahendamisaja sõltuvus andmemahust erinevate meetodite korral):
# x-teljel on andmemaht, y-teljel lahendusaeg millisekundites.
# Kui kiirete meetodite jooned on graafikul halvasti eristuvad,
# siis kasutage logaritmilist skaalat y-telje kuvamiseks.
# Mõõta ainult järjestamiseks kuluvat aega iga meetodi jaoks võimalikult segamatult.
# Parema tulemuse saate, kui teete mitu mõõtmist ning leiate tulemustest keskmise.
# Õppejõule näidata nii programmi kui ka graafikut praktikumi lõpus.

import random
import time
from sortimine import sortimine
from prettytable import PrettyTable
import matplotlib.pyplot as plt





def sortimiseAndmed(n):
# tekitab etteantud pikkusega massiivi
# sisend n - andmemahu suurus
    andmed = []
    i = 0
    while i < n:
        andmed.append(random.randint(0,256))
        i += 1

    return(andmed)



def prooviSortimiseAlgoritmid(lahteAndmed,n,iteratsioon):
    # proovib läbi kõik sortimise algoritmid
    # sisend:
    # lahteAndmed = list
    # n = andmemahu suurus
    # iteratsioon = mitmendat iteratsiooni tehakse

    pikkus = len(lahteAndmed)

    # insertionsort
    algus = time.perf_counter()
    sortimine.insertionsort(lahteAndmed)
    lopp = time.perf_counter()
    #print("Pistemeetod\t" + str(iteratsioon) + "\t" + str(n) + "\t" + str(lopp-algus))
    timing1 = lopp-algus

    # biinsertionsort
    algus = time.perf_counter()
    sortimine.binsertionsort(lahteAndmed)
    lopp = time.perf_counter()
    #print("Kahendpistemeetod\t"  + str(iteratsioon) + "\t" +  str(n) + "\t" + str(lopp-algus))
    timing2 = lopp - algus

    # quicksort
    algus = time.perf_counter()
    sortimine.quicksort(lahteAndmed, 0, pikkus)
    lopp = time.perf_counter()
    #print("Kiirmeetod\t" + str(iteratsioon) + "\t" + str(n) + "\t" + str(lopp-algus))
    timing3 = lopp - algus

    # ühildamismeetod
    algus = time.perf_counter()
    sortimine.mergesort(lahteAndmed, 0, pikkus)
    lopp = time.perf_counter()
    #print("Ühildamismeetod\t" + str(iteratsioon) + "\t" + str(n) + "\t" + str(lopp-algus))
    timing4 = lopp - algus

    # api meetod
    algus = time.perf_counter()
    lahteAndmed.sort()
    lopp = time.perf_counter()
    #print("API\t" + str(iteratsioon) + "\t" +  str(n) + "\t" + str(lopp-algus))
    timing5 = lopp - algus

    # # radix
    # algus = time.perf_counter()
    # sortimine.radixsort(lahteAndmed)
    # lopp = time.perf_counter()
    # print("Radix\t" + str(n) + "\t" + str(lopp-algus))

    return([timing1, timing2, timing3, timing4, timing5])


def keskmine(endineKeskmine,lisatav,element):

    keskmine = (endineKeskmine * (element - 1) + lisatav) / element

    return(keskmine)

# parameetrid

maxKordusteArv = 2
failinimi = "timing.txt"

pistemeetod = []
kahendpistemeetod = []
kiirmeetod = []
yhildamismeetod = []
apiMeetod = []


# teeme tühja listi
tulemused = [None] * (10 * maxKordusteArv)

# Salvestame faili esimese rea
f = open(failinimi, "w")
f.write("Iteratsioon,Andmemaht,Pistemeetod,Kahendpistemeetod,Kiirmeetod,Ühildamismeetod,API\n")
f.close()


# proovime läbi kõik algoritmid 5 korda
# iga iteratsiooni jaoks teeme uue dataseti
kordus = 1
while kordus <= maxKordusteArv:
    print()

    # teeme algandmed
    andmed = sortimiseAndmed(64000)
    mahud = [1000, 2000, 4000, 8000, 16000]
    #mahud = [100, 200, 300, 400, 500, 600, 700, 800, 900, 1000, 1100] # testimiseks väikesed mahud
    #mahud = [100, 200, 300] # testimiseks väikesed mahud


    for i in range(len(mahud)):
        print('Test nr: ' + str(kordus) + '. Datset ' + str(mahud[i]) + '...', end='')
        timingTulemus = prooviSortimiseAlgoritmid(andmed[0:mahud[i]],mahud[i],kordus)
        print("Done")
        #print(timingTulemus)


        # lisame testi tulemused iga meetodi listi
        # kui on rohkem kui 1 kordust, siis arvutame keskmise
        if kordus > 1:
            pistemeetod[i] = keskmine(pistemeetod[i],timingTulemus[0],kordus)
            kahendpistemeetod[i] = keskmine(kahendpistemeetod[i],timingTulemus[1],kordus)
            kiirmeetod[i] = keskmine(kiirmeetod[i],timingTulemus[2],kordus)
            yhildamismeetod[i] = keskmine(yhildamismeetod[i],timingTulemus[3],kordus)
            apiMeetod[i] = keskmine(apiMeetod[i],timingTulemus[4],kordus)
        else:
            pistemeetod.append(timingTulemus[0])
            kahendpistemeetod.append(timingTulemus[1])
            kiirmeetod.append(timingTulemus[2])
            yhildamismeetod.append(timingTulemus[3])
            apiMeetod.append(timingTulemus[4])

        # Salvestame rea faili
        f = open(failinimi, "a")
        f.write(str(kordus) + "," + str(mahud[i]) + ",")
        for i in range(len(timingTulemus)):
            f.write(str(timingTulemus[i]))
            if i < len(timingTulemus) - 1:
                f.write(",")
        f.write("\n")
        f.close()

    kordus += 1



tabel = PrettyTable()

column_names = ["Andmemaht","Pistemeetod","Kahendpistemeetod","Kiirmeetod","Ühildamismeetod","API"]

tabel.add_column(column_names[0], mahud)
tabel.add_column(column_names[1], pistemeetod)
tabel.add_column(column_names[2], kahendpistemeetod)
tabel.add_column(column_names[3], kiirmeetod)
tabel.add_column(column_names[4], yhildamismeetod)
tabel.add_column(column_names[5], apiMeetod)

print(tabel)

plt.plot(mahud, pistemeetod, color='g', label='Pistemeetod')
plt.plot(mahud, kahendpistemeetod, color='orange', label='Kahendpistemeetod')
plt.plot(mahud, kiirmeetod, color='blue', label='Kiirmeetod')
plt.plot(mahud, yhildamismeetod, color='red', label='Ühildamismeetod')
plt.plot(mahud, apiMeetod, color='black', label='API')
plt.xlabel('Maht')
plt.ylabel('Aeg (s)')
plt.yscale('log')
plt.title('Sortimisalgoritmide võrdlus')
plt.legend(loc=2)
plt.show()