


def bsearch(arr, value):
# Kahendotsimine (Binary Search)

    n = len(arr)
    if n == 0:
        return -1
    if value > arr[n - 1]:
        return -1
    left = 0
    right = len(arr)
    while left <= right:
        mid = int((left + right) / 2)
        x = arr[mid]
        if value == x:
            return mid
        else:
            if value > x:
                left = mid + 1
            else:
                right = mid - 1
    return -1


def bubblesort(arr):
# mullimeetod

    if len(arr) < 2:
        return
    unsorted = True
    while unsorted:
        unsorted = False
        for i in range(1, len(arr)):
            if arr[i-1] > arr[i]:
                unsorted = True
                tmp = arr[i-1]
                arr[i-1] = arr[i]
                arr[i] = tmp


def selectionsort(arr):
# valikumeetod

    if len(arr) < 2:
        return
    for m in range(0, len(arr) - 1):
        minind = m
        minel = arr[m]
        for i in range(m+1, len(arr)):
            if arr[i] < arr[minind]:
                minind = i
                minel = arr[i]
        arr[m+1:minind+1] = arr[m:minind]
        arr[m] = minel


def insertionsort(arr):
# Pistemeetod(Insertion Sort)
# Jada jagatakse "järjestatud osaks" (algselt tühi) ja "järjestamata osaks" (algselt kogu jada).
# Järjestatud osa pikkust suurendatakse järjekordse elemendi paigutamisega õigele kohale järjestatud osas.
    if len(arr) < 2:
        return
    for i in range(1, len(arr)):
        b = arr[i]
        j = i - 1
        while j >= 0:
            if arr[j] <= b:
                break
            arr[j+1] = arr[j]
            j -= 1
        arr[j+1] = b



def binsertionsort(arr):
# Kahendpistemeetod:
# "Õige" koht leitakse kahendotsimisega: O(nlogn), kui "pistmine" oleks O(1)

    if len(arr) < 2:
        return
    for i in range(1, len(arr)):
        b = arr[i]
        left = 0
        right = i
        while left < right:
            mid = int((left + right) / 2)
            if b < arr[mid]:
                right = mid
            else:
                left = mid + 1
        arr[left+1:i+1] = arr[left:i]
        arr[left] = b


def mergesort(arr, left, right):
# Ühildusmeetod (Merge Sort):
# "Jaga ja valitse" : jagatakse kogu aeg pooleks (kuni pikkus < 2),
# pooled ühendatakse lineaarse keerukusega ühildamise abil, kasutatakse lisamälu mahuga O(n)

    if len(arr) < 2 or (right - left) < 2:
        return
    k = int((left + right)/2)
    mergesort(arr, left, k)
    mergesort(arr, k, right)
    merge(arr, left, k, right)


def merge(arr, left, k, right):
    if len(arr) < 2 or (right - left) < 2 or k <= left or k >= right:
        return
    tmp = [0] * (right - left)
    n1 = left
    n2 = k
    m = 0
    while True:
        if n1 < k and n2 < right:
            if arr[n1] > arr[n2]:
                tmp[m] = arr[n2]
                n2 += 1
            else:
                tmp[m] = arr[n1]
                n1 += 1
            m += 1
        else:
            if n1 >= k:
                tmp[m:] = arr[n2:right]
            else:
                tmp[m:] = arr[n1:k]
            break
    arr[left:right] = tmp[:]


def quicksort(arr, left, right):
# Kiirmeetod (Quicksort):
# (Osa)jada jagatakse kaheks lühemaks osajadaks nii, et ükski element esimeses osas
# ei oleks suurem ühestki elemendist teises osas. Siis võib kummagi osa sorteerida
# eraldi (nad on sõltumatud). "Jaga ja valitse"
    if (right - left) < 2:
        return
    x = arr[int((left + right) / 2)]
    i = left
    j = right - 1
    while i < j:
        while arr[i] < x:
            i += 1
        while arr[j] > x:
            j -= 1
        if i > j:
            break
        tmp = arr[i]
        arr[i] = arr[j]
        arr[j] = tmp
        i += 1
        j -= 1
    if left < j:
        quicksort(arr, left, j + 1)
    if i < right - 1:
        quicksort(arr, i, right)


# =======================================================================
#  Author: Isai Damier
#  Title: Radix Sort
#  Project: geekviewpoint
#  Package: algorithms
#
#  Statement:
#  Given a disordered list of integers, rearrange them in natural order.
#
#  Sample Input: [18,5,100,3,1,19,6,0,7,4,2]
#
#  Sample Output: [0,1,2,3,4,5,6,7,18,19,100]
#
#  Time Complexity of Solution:
#  Best Case O(kn); Average Case O(kn); Worst Case O(kn),
#  where k is the length of the longest number and n is the
#  size of the input array.
#
#  Note: if k is greater than log(n) then an nlog(n) algorithm would
#  be a better fit. In reality we can always change the radix
#  to make k less than log(n).
#
#  Approach:
#  radix sort, like counting sort and bucket sort, is an integer based
#  algorithm (i.e. the values of the input array are assumed to be
#  integers). Hence radix sort is among the fastest sorting algorithms
#  around, in theory. The particular distinction for radix sort is
#  that it creates a bucket for each cipher (i.e. digit); as such,
#  similar to bucket sort, each bucket in radix sort must be a
#  growable list that may admit different keys.
#
#  For decimal values, the number of buckets is 10, as the decimal
#  system has 10 numerals/cyphers (i.e. 0,1,2,3,4,5,6,7,8,9). Then
#  the keys are continuously sorted by significant digits.
# =======================================================================
def radixsort(aList):
    RADIX = 10
    maxLength = False
    tmp, placement = -1, 1

    while not maxLength:
        maxLength = True
        # declare and initialize buckets
        buckets = [list() for _ in range(RADIX)]

        # split aList between lists
        for i in aList:
            tmp = i / placement
            buckets[tmp % RADIX].append(i)
            if maxLength and tmp > 0:
                maxLength = False

        # empty lists into aList array
        a = 0
        for b in range(RADIX):
            buck = buckets[b]
            for i in buck:
                aList[a] = i
                a += 1

        # move to next digit
        placement *= RADIX