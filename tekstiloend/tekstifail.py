# Koostada programm, mis loob järjendi kõigist etteantud tekstifaili
# (vaikimisi standardsisendist saadud) sõnadest.
# Sõnadevaheliseks eraldajaks on kõik whitespace-sümbolid või nende jadad.
# Eelmises punktis moodustatud järjendist moodustada sõnade esinemise sagedustabel sõnastikuna
# (paarid "sõna - selle esinemiste arv"). Väljastada sõnastiku elemendid võtmete tähestikulises järjestuses
# (iga paar eraldi real).


import sys

if len(sys.argv) > 1:
    f = open(sys.argv[1], "r")
else:
    file = sys.stdin


#f = open("tekstifail2.txt", "r")




#laeme leitud sõnad listi
sonad = []
sonad_reas =[]

for line in f:
     sonad_reas = (line.rstrip('\n').split())
     for i in sonad_reas:
         sonad.append(i)

f.close()


sagedustabel = {}
#sonad.sort()

for i in range(len(sonad)):
    if sonad[i] not in sagedustabel.keys():
        sagedustabel[sonad[i]] = sonad.count(sonad[i])


for key, value in sorted(sagedustabel.items()):
    print(key + " : " + str(value))